MARK ?= not slowtest
KEYWORD ?= test
ARGS ?=

test: activate-service-account
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

slowtest:
	$(MAKE) MARK='slowtest' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
REPO_NAME ?= rhedcloud-gcp-vpc-type1-dm
PROJECT_ID ?= rhedcloud-pipeline-project-2
PROJECT_NUMBER ?= 696182218797
DEPLOYMENT_NAME ?= rhedcloud-gcp-vpc-type1
RS_PROJECT_DEPLOYMENT_NAME ?= rhedcloud-gcp-rs-project
BITBUCKET_BUILD_NUMBER ?= 0
GOOGLE_APPLICATION_CREDENTIALS ?= /tmp/key_file
BB_TEAM ?= rhedcloud

save-creds:
	save_api_keys.sh

activate-service-account:
	gcloud auth activate-service-account --key-file "$(GOOGLE_APPLICATION_CREDENTIALS)"
	gcloud config set project $(PROJECT_ID)
	$(MAKE) whoami

whoami:
	gcloud config list account --format "value(core.account)"

# Download the artifacts required for the tests in this repository
get-artifacts:
	download_artifact.sh rhedcloud-gcp-rs-project-dm rhedcloud-gcp-rs-project-dm.latest.zip

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
rebuild: get-artifacts clean rs-project vpc-type1

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean rs-project vpc-type1

# Clean up resources if stack changes are detected
clean: activate-service-account
	delete_deployment.py "$(DEPLOYMENT_NAME)"
	delete_deployment.py "$(RS_PROJECT_DEPLOYMENT_NAME)"

# Create the rs-project deployment
rs-project: activate-service-account
	$(MAKE) -C rhedcloud-gcp-rs-project-dm \
		DEPLOYMENT_NAME=$(RS_PROJECT_DEPLOYMENT_NAME) \
		PROJECT_ID=$(PROJECT_ID) \
		PROJECT_NUMBER=$(PROJECT_NUMBER) \
		rs-project

shared-secret:
ifndef $(SHARED_SECRET)
	$(eval SHARED_SECRET := $(shell openssl rand -hex 24))
	echo -n $(SHARED_SECRET) | gcloud beta secrets create $(BITBUCKET_BUILD_NUMBER) --replication-policy=automatic --quiet --data-file=-
endif
	@echo "Shared secret: $(SHARED_SECRET)"

# Create the vpc-type1 deployment
vpc-type1: activate-service-account working-vpc-type1
	cat "$(DEPLOYMENT_NAME)-dm-working.json"
	gcloud deployment-manager deployments create "$(DEPLOYMENT_NAME)" \
		--project "$(PROJECT_ID)" \
		--config "$(DEPLOYMENT_NAME)-dm-working.json"

# Replace various placeholders in the deployment template with the appropriate values.
working-vpc-type1: shared-secret
	sed -e "s/BITBUCKET_BUILD_NUMBER/$(BITBUCKET_BUILD_NUMBER)/g" \
		-e "s/PROJECT_ID/$(PROJECT_ID)/g" \
		-e "s/PROJECT_NUMBER/$(PROJECT_NUMBER)/g" \
		-e "s/SHARED_SECRET/$(SHARED_SECRET)/g" \
		"$(DEPLOYMENT_NAME)-dm.json" > "$(DEPLOYMENT_NAME)-dm-working.json"

# Upload test artifacts
upload:
	git archive --format zip --output $(REPO_NAME).latest.zip master
	cp $(REPO_NAME).json $(REPO_NAME).latest.json
	upload_artifact.sh $(REPO_NAME).latest.json $(REPO_NAME).latest.zip

.EXPORT_ALL_VARIABLES:
