def test_address_count(api_client, project_id, region):
    """Verify the expected number of addresses.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.addresses().list(
        project=project_id,
        region=region,
    ).execute()

    assert len(res["items"]) == 1


def test_vpc_gateway_address(api_client, project_id, region):
    """Verify information about a VPN Address.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.addresses().get(
        project=project_id,
        region=region,
        address="vpn-gateway-address",
    ).execute()

    assert res["addressType"] == "EXTERNAL"
    assert res["networkTier"] == "PREMIUM"
    assert res["status"] == "IN_USE"
