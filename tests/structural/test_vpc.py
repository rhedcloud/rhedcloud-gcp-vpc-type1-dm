import pytest


@pytest.dict_fixture
def shared_vars():
    return {
        "vpc": None,
        "vpc_id": None,
    }


def test_vpc_exists(api_client, project_id, shared_vars):
    """Verify that a VPC exists.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param dict shared_vars:
        A collection used to share data across tests.

    """

    res = api_client.networks().get(
        project=project_id,
        network="vpc-1",
    ).execute()

    assert res["autoCreateSubnetworks"] is False
    assert res["routingConfig"]["routingMode"] == "REGIONAL"

    shared_vars["vpc"] = res
    shared_vars["vpc_id"] = res["id"]


@pytest.mark.xfail(reason="Need more information")
def test_vpc_status(api_client, project_id, vpc_id):
    assert False


@pytest.mark.xfail(reason="Need more information")
def test_vpc_type(api_client, project_id, vpc_id):
    assert False


@pytest.mark.xfail(reason="Need more information")
def test_vpc_cidr(api_client, project_id, vpc):
    expected = {
        "10.64.0.0/16",
        "10.65.0.0/16",
        "10.66.0.0/16",
        "10.67.0.0/16",
        "10.68.0.0/16",
    }

    assert False


@pytest.mark.parametrize("subnet_name, cidr", (
    ("management1", "10.66.142.0/26"),
    ("private1", "10.66.142.64/26"),
    ("public1", "10.66.142.128/25"),
))
def test_subnetwork(api_client, project_id, region, subnet_name, cidr):
    """Verify configuration of a VPC's subnetworks.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.
    :param str subnet_name:
        Name of a specific subnetwork to inspect.
    :param str cidr:
        Expected CIDR range for the subnetwork.

    """

    res = api_client.subnetworks().get(
        project=project_id,
        region=region,
        subnetwork=subnet_name,
    ).execute()

    assert res["privateIpGoogleAccess"] is True
    assert res["enableFlowLogs"] is True
    assert res["ipCidrRange"] == cidr
