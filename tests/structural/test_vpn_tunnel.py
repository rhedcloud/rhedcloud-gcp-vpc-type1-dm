import pytest


def test_vpn_tunnel_count(api_client, project_id, region):
    """Verify the expected number of VPN Tunnels.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.vpnTunnels().list(
        project=project_id,
        region=region,
    ).execute()

    assert len(res["items"]) == 1


@pytest.mark.xfail(reason="NO_INCOMING_PACKETS")
def test_vpn_tunnel_info(api_client, project_id, region):
    """Verify information about a VPN Tunnel.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.vpnTunnels().get(
        project=project_id,
        region=region,
        vpnTunnel="vpn-tunnel",
    ).execute()

    assert res["ikeVersion"] == 2
    assert res["peerIp"] == "35.224.168.201"
    assert res["localTrafficSelector"] == ["10.1.0.0/24"]
    assert res["remoteTrafficSelector"] == ["10.128.0.0/20"]

    assert res["status"] in (
        "PROVISIONING",
        "WAITING_FOR_FULL_CONFIG",
        "FIRST_HANDSHAKE",
        "ESTABLISHED",
    )
