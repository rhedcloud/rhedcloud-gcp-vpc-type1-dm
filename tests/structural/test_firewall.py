import pytest


@pytest.fixture
def vpc_url(project_id):
    return "https://www.googleapis.com/compute/v1/projects/{}/global/networks/vpc-1".format(project_id)


def test_firewall_count(api_client, project_id, vpc_url):
    """Verify the expected number of firewalls.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str vpc_url:
        Full URL of the VPC to inspect.

    """

    res = api_client.firewalls().list(
        project=project_id,
        filter='(network = "{}")'.format(vpc_url),
    ).execute()

    assert len(res["items"]) == 1


def test_firewall_rule(api_client, project_id):
    """Verify information about a firewall rule.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.

    """

    res = api_client.firewalls().get(
        project=project_id,
        firewall="allow-web-traffic",
    ).execute()

    assert len(res["allowed"]) == 1
    assert res["priority"] == 1000
    assert res["sourceRanges"] == ["10.120.0.0/16"]
    assert res["direction"] == "INGRESS"
    assert res["disabled"] is False

    rule = res["allowed"][0]
    assert rule["IPProtocol"] == "tcp"
    assert rule["ports"] == ["80", "443"]
