import pytest


@pytest.fixture(scope="module")
def api():
    return "compute"


@pytest.fixture(scope="module")
def sa_scopes():
    return (
        "https://www.googleapis.com/auth/cloud-platform.read-only",
        "https://www.googleapis.com/auth/compute.readonly",
    )
