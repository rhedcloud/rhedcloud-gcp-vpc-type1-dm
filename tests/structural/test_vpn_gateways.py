def test_vpn_gateway_count(api_client, project_id, region):
    """Verify the expected number of VPN gateways.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.targetVpnGateways().list(
        project=project_id,
        region=region,
    ).execute()

    assert len(res["items"]) == 1


def test_vpn_gateway_info(api_client, project_id, region):
    """Verify information about a VPN gateway.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.targetVpnGateways().get(
        project=project_id,
        region=region,
        targetVpnGateway="vpn-gateway",
    ).execute()

    assert res["status"] in ("CREATING", "READY")
    assert len(res["tunnels"]) == 1
    assert len(res["forwardingRules"]) == 3
