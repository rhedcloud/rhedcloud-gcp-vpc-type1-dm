import pytest


def test_forwardingrules_count(api_client, project_id, region):
    """Verify the expected number of forwarding rules.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.

    """

    res = api_client.forwardingRules().list(
        project=project_id,
        region=region,
    ).execute()

    assert len(res["items"]) == 3


@pytest.mark.parametrize("rule_name, protocol, port_range", (
    ("esp-rule", "ESP", None),
    ("udp-4500-rule", "UDP", 4500),
    ("udp-500-rule", "UDP", 500),
))
def test_forwardingrule_info(api_client, project_id, region, rule_name, protocol, port_range):
    """Verify information about forwarding rules.

    :param api_client:
        GCP API client.
    :param str project_id:
        Current GCP Project ID.
    :param str region:
        GCP region to use for testing.
    :param str rule_name:
        Name of a forwarding rule to inspect.
    :param str protocol:
        Expected protocol for the forwarding rule.
    :param port_range:
        Expected port range for the forwarding rule. If ``None``, verify that
        no port range is present on the forwarding rule. If an integer, use
        that integer as the start and end port of a port range.

    """

    res = api_client.forwardingRules().get(
        project=project_id,
        region=region,
        forwardingRule=rule_name,
    ).execute()

    assert res["IPProtocol"] == protocol

    if port_range is None:
        assert "portRange" not in res
    else:
        if isinstance(port_range, int):
            port_range = "{0}-{0}".format(port_range)
        assert res["portRange"] == port_range

    assert res["networkTier"] == "PREMIUM"
    assert res["loadBalancingScheme"] == "EXTERNAL"
